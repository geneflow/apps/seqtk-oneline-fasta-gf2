SeqTK One Line Fasta GeneFlow App
=================================

Version: 1.3-01

This GeneFlow app wraps the SeqTK tool to generate a one line fasta file.

Inputs
------

1. input: Fasta File.

Parameters
----------

1. output: Output Fasta File.
